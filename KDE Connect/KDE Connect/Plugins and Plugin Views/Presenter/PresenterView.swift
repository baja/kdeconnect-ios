/*
 * SPDX-FileCopyrightText: 2021 Lucas Wang <lucas.wang@tuta.io>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

// Original header below:
//
//  PresenterView.swift
//  KDE Connect Test
//
//  Created by Lucas Wang on 2021-09-13.
//

import SwiftUI

struct PresenterView: View {
    let detailsDeviceId: String
    @State var currPointerX: Float = 0.0
    @State var currPointerY: Float = 0.0
    
    @State private var pointerSensitivityFromSlider: Float = 3.0 // defaults to the middle
    @State private var showingSensitivitySlider: Bool = false
    
    var body: some View {
        VStack {
            if backgroundService._devices[detailsDeviceId]!._type == DeviceType.Desktop {
                Image(systemName: "wand.and.rays")
                    .resizable()
                    .frame(width: 110, height: 110)
                    .foregroundColor(.white)
                    .padding(EdgeInsets(top: 130, leading: 130, bottom: 130, trailing: 130))
                    .background(Color.orange)
                    .clipShape(Rectangle())
                    .cornerRadius(50)
                    .gesture(
                        DragGesture(minimumDistance: 0)
                            .onChanged({ _ in
                                startGyroAndPointer()
                            })
                            .onEnded({ _ in
                                stopGyroAndPointer()
                            })
                    )
            }
            
            HStack {
                Button(action: {
                    goBackAction()
                }, label: {
                    Image(systemName: "backward.end")
                        .resizable()
                        .frame(width: 40, height: 50)
                        .foregroundColor(.white)
                    // TODO: reduce duplication
                        .padding(EdgeInsets(top: (backgroundService._devices[detailsDeviceId]!._type == DeviceType.Desktop) ? 30 : 200, leading: 70, bottom: (backgroundService._devices[detailsDeviceId]!._type == DeviceType.Desktop) ? 30 : 200, trailing: 70))
                        .background(Color.orange)
                        .clipShape(Rectangle())
                        .cornerRadius(20)
                })
                
                Button(action: {
                    goForwardAction()
                }, label: {
                    Image(systemName: "forward.end")
                        .resizable()
                        .frame(width: 40, height: 50)
                        .foregroundColor(.white)
                        .padding(EdgeInsets(top: (backgroundService._devices[detailsDeviceId]!._type == DeviceType.Desktop) ? 30 : 200, leading: 70, bottom: (backgroundService._devices[detailsDeviceId]!._type == DeviceType.Desktop) ? 30 : 200, trailing: 70))
                        .background(Color.orange)
                        .clipShape(Rectangle())
                        .cornerRadius(20)
                })
            }
            
            if (showingSensitivitySlider) {
                VStack {
                    HStack {
                        // TODO: replace independent images as part of slider
                        Image(systemName: "minus")
                        Slider(
                            value: $pointerSensitivityFromSlider,
                            in: 0.5...5.5,
                            onEditingChanged: { editing in
                                if (!editing) {
                                    hapticGenerators[Int(HapticStyle.rigid.rawValue)].impactOccurred()
                                    saveDeviceToUserDefaults(deviceId: detailsDeviceId)
                                }
                            }
                        )
                        .onChange(of: pointerSensitivityFromSlider) { value in
                            backgroundService._devices[detailsDeviceId]!._pointerSensitivity = value
                        }
                        Image(systemName: "plus")
                    }
                    Text("Pointer Sensitivity")
                }
                .padding(.all, 15)
                .transition(.opacity)
            }
            
        }
        .navigationBarTitle("PresenterView.NavigationTitle", displayMode: .inline)
        .navigationBarItems(trailing: {
            Menu {
                Button(action: {
                    goFullscreenAction()
                }, label: {
                    HStack {
                        Text("PresenterView.FullScreen")
                        Image(systemName: "arrow.up.left.and.arrow.down.right")
                    }
                })
                
                Button(action: {
                    goEscapeAction()
                }, label: {
                    HStack {
                        Text("PresenterView.ExitPresentation")
                        Image(systemName: "arrowshape.turn.up.left")
                    }
                })
                
                Button(action: {
                    withAnimation {
                        showingSensitivitySlider.toggle()
                    }
                }, label: {
                    HStack {
                        Text("PresenterView.SensitivitySlider \((showingSensitivitySlider) ? String(localized: "PresenterView.SensitivitySlider.Hide") : String(localized: "PresenterView.SensitivitySlider.Show"))")
                        Image(systemName: "cursorarrow.motionlines")
                    }
                })
                
            } label: {
                Image(systemName: "ellipsis.circle")
            }
        }())
    }
    
    func startGyroAndPointer() -> Void {
        //hapticGenerators[Int(HapticStyle.heavy.rawValue)].impactOccurred()
        motionManager.startGyroUpdates(to: .main) { (data, error) in
            if (data != nil) {
                (backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_PRESENTER] as! Presenter).sendPointerPosition(Dx: Float(data!.rotationRate.x) * pointerSensitivityFromSlider, Dy: Float(data!.rotationRate.y) * pointerSensitivityFromSlider)
            }
        }
    }
    
    func stopGyroAndPointer() -> Void {
        //hapticGenerators[Int(HapticStyle.heavy.rawValue)].impactOccurred()
        (backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_PRESENTER] as! Presenter).sendStopPointer()
        motionManager.stopGyroUpdates()
    }
    
    func goFullscreenAction() -> Void {
        notificationHapticsGenerator.notificationOccurred(.success)
        (backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_PRESENTER] as! Presenter).sendFullscreen()
    }
    
    func goEscapeAction() -> Void {
        notificationHapticsGenerator.notificationOccurred(.warning)
        (backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_PRESENTER] as! Presenter).sendEsc()
    }
    
    func goBackAction() -> Void {
        hapticGenerators[Int(HapticStyle.soft.rawValue)].impactOccurred()
        (backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_PRESENTER] as! Presenter).sendPrevious()
    }
    
    func goForwardAction() -> Void {
        hapticGenerators[Int(HapticStyle.rigid.rawValue)].impactOccurred()
        (backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_PRESENTER] as! Presenter).sendNext()
    }
}

//struct PresenterView_Previews: PreviewProvider {
//    static var previews: some View {
//        PresenterView(detailsDeviceId: "Hi")
//    }
//}
