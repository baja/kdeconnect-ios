/*
 * SPDX-FileCopyrightText: 2021 Lucas Wang <lucas.wang@tuta.io>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

// Original header below:
//
//  ConfigureDeviceByIPView.swift
//  KDE Connect Test
//
//  Created by Lucas Wang on 2021-09-03.
//

import SwiftUI
import UIKit

struct ConfigureDeviceByIPView: View {
    @ObservedObject var selfDeviceDataForIPConfig: SelfDeviceData = selfDeviceData
    @State var showingAddNewIPAlert: Bool = false
    
    var body: some View {
        ZStack {
            EmptyView()
                .alert(isPresented: $showingAddNewIPAlert,
                       TextAlert(title: String(localized: "ConfigureDeviceByIPView.Empty.Title"),
                                 message: String(localized: "ConfigureDeviceByIPView.Empty.Message")) { result in
                    if let address = result {
                        // address was accepted
                        withAnimation {
                            selfDeviceDataForIPConfig.directIPs.append(address)
                        }
                    } else {
                        // The alert was cancelled
                    }
                })
            List {
                Section(header: Text("ConfigureDeviceByIPView.DirectHandshakeDevices.Header"), footer: Text("ConfigureDeviceByIPView.DirectHandshakeDevices.Footer")) {
                    ForEach(selfDeviceDataForIPConfig.directIPs, id: \.self) { address in
                        Text(address) // maybe add ability to edit as well?????
                    }
                    .onDelete(perform: deleteAddress)
                }
            }
        }
        .navigationTitle("ConfigureDeviceByIPView.NavigationTitle")
        .navigationBarItems(trailing: Button(action: {
            showingAddNewIPAlert = true
        }) {
            Image(systemName: "plus")
        })
    }
    func deleteAddress(at offsets: IndexSet) {
        selfDeviceDataForIPConfig.directIPs.remove(atOffsets: offsets)
    }
}

//struct ConfigureDeviceByIPView_Previews: PreviewProvider {
//    static var previews: some View {
//        ConfigureDeviceByIPView()
//    }
//}
