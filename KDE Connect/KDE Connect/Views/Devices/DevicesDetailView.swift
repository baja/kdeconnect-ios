/*
 * SPDX-FileCopyrightText: 2021 Lucas Wang <lucas.wang@tuta.io>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

// Original header below:
//
//  DevicesDetailView.swift
//  KDE Connect Test
//
//  Created by Lucas Wang on 2021-06-17.
//

import SwiftUI
import UniformTypeIdentifiers
import UIKit

struct DevicesDetailView: View {
    let detailsDeviceId: String
    @State var showingEncryptionInfo: Bool = false
    @State private var showingUnpairConfirmationAlert: Bool = false
    @State private var showingFilePicker: Bool = false
    @State var isStilConnected: Bool = true
    @State private var showingPluginSettingsView: Bool = false
    
    @State var chosenFileURLs: [URL] = []
    
    // TODO: Maybe use a state to directly change the Battery % instead of doing this hacky thing?
    @State var viewUpdate: Bool = false
    
    var body: some View {
        if (isStilConnected) {
            VStack {
                List {
                    Section(header: Text("DevicesDetailView.Actions.Title")) {
                        if ((backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_CLIPBOARD] != nil) && backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_CLIPBOARD] as! Bool) {
                            Button(action: {
                                (backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_CLIPBOARD] as! Clipboard).sendClipboardContentOut()
                            }, label: {
                                Label("DevicesDetailView.Actions.PushLocalClipboard", systemImage: "square.and.arrow.up.on.square.fill")
                            })
                                .accentColor(.primary)
                        }
                        
                        if ((backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_SHARE] != nil) && backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_SHARE] as! Bool) {
                            Button(action: {
                                showingFilePicker = true
                            }, label: {
                                Label("DevicesDetailView.Actions.SendFiles", systemImage: "folder")
                            })
                                .accentColor(.primary)
                        }
                        
                        if ((backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_PRESENTER] != nil) && backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_PRESENTER] as! Bool) {
                            NavigationLink(
                                destination: PresenterView(detailsDeviceId: detailsDeviceId),
                                label: {
                                    Label("DevicesDetailView.Actions.SlideshowRemote", systemImage: "slider.horizontal.below.rectangle")
                                })
                        }
//
//                        NavigationLink(
//                            destination: PlaceHolderView(),
//                            label: {
//                                HStack {
//                                    Image(systemName: "playpause")
//                                    Text("Multimedia control")
//                                }
//                            })
//
                        
                        if ((backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_RUNCOMMAND] != nil) && backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_RUNCOMMAND] as! Bool) {
                            NavigationLink(
                                destination: RunCommandView(detailsDeviceId: self.detailsDeviceId),
                                label: {
                                    Label("DevicesDetailView.Actions.RunCommand", systemImage: "terminal")
                                })
                        }
                        
                        if ((backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_MOUSEPAD_REQUEST] != nil) && backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_MOUSEPAD_REQUEST] as! Bool) {
                            NavigationLink(
                                destination: RemoteInputView(detailsDeviceId: self.detailsDeviceId),
                                label: {
                                    Label("DevicesDetailView.Actions.RemoteInput", systemImage: "hand.tap")
                                })
                        }
                    }
                    
                    Section(header: Text("DevicesDetailView.DeviceStatus.Title")) {
                        if ((backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_BATTERY_REQUEST] == nil) || ((backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_BATTERY_REQUEST] as! Battery).remoteChargeLevel == 0)) {
                            Text("DevicesDetailView.DeviceStatus.Battery.NotFound")
                        } else if (!(backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_BATTERY_REQUEST] as! Bool)) {
                            Text("DevicesDetailView.DeviceStatus.Battery.Plugin.Disabled")
                        } else {
                            HStack {
                                Label {
                                    Text("DevicesDetailView.DeviceStatus.Battery.Level")
                                } icon: {
                                    Image(systemName: (backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_BATTERY_REQUEST] as! Battery).getSFSymbolNameFromBatteryStatus())
                                        .accentColor((backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_BATTERY_REQUEST] as! Battery).getSFSymbolColorFromBatteryStatus())
                                }
                                Spacer()
                                Text("\((backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_BATTERY_REQUEST] as! Battery).remoteChargeLevel)%")
                            }
                        }
                    }
                    
//                    Section(header: Text("Debug section")) {
//                        Text("Chosen file URLs:")
//                        ForEach(chosenFileURLs, id: \.self) { url in
//                            Text(url.absoluteString)
//                        }
//                    }
                    
                }
                .environment(\.defaultMinListRowHeight, 50) // TODO: make this dynamic with GeometryReader???
                .alert("DevicesDetailView.EncryptionInfo.Title", isPresented: $showingEncryptionInfo) {} message: {
                    Text("DevicesDetailView.EncryptionInfo.Message \((certificateService.hostCertificateSHA256HashFormattedString == nil) ? "ERROR" : certificateService.hostCertificateSHA256HashFormattedString!) \((backgroundService._devices[detailsDeviceId]!._SHA256HashFormatted == nil || backgroundService._devices[detailsDeviceId]!._SHA256HashFormatted == "") ? String(localized: "DevicesDetailView.EncryptionInfo.Message.With.Device.Error") : backgroundService._devices[detailsDeviceId]!._SHA256HashFormatted)")
                }
                .alert("DevicesDetailView.Unpair.Title", isPresented: $showingUnpairConfirmationAlert) {
                    Button("DevicesDetailView.Unpair.No", role: .cancel) {}
                    Button("DevicesDetailView.Unpair.Yes", role: .destructive) {
                        backgroundService.unpairDevice(detailsDeviceId)
                        isStilConnected = false
//                        backgroundService.refreshDiscovery()
//                        connectedDevicesViewModel.onDeviceListRefreshed()
                    }
                } message: {
                    Text("DevicesDetailView.Unpair.Message \(backgroundService._devices[detailsDeviceId]!._name)")
                }
                
                NavigationLink(destination: DeviceDetailPluginSettingsView(detailsDeviceId: self.detailsDeviceId), isActive: $showingPluginSettingsView) {
                    EmptyView()
                }
                
                // This is an invisible view using changes in viewUpdate to force SwiftUI to re-render the entire screen. We want this because the battery information is NOT a @State variables, as such in order for updates to actually register, we need to force the view to re-render
                Text(viewUpdate ? "True" : "False")
                    .frame(width: 0, height: 0)
                    .opacity(0)
                
            }
            .navigationTitle(backgroundService._devices[detailsDeviceId]!._name)
            .navigationBarItems(trailing: {
                Menu {
                    if ((backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_PING] != nil) && backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_PING] as! Bool) {
                        Button(action: {
                            (backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_PING] as! Ping).sendPing()
                        }, label: {
                            HStack {
                                Text("DevicesDetailView.Actions.SendPing")
                                Image(systemName: "megaphone")
                            }
                        })
                    }
                    
                    if ((backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_FINDMYPHONE_REQUEST] != nil) && backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_FINDMYPHONE_REQUEST] as! Bool) {
                        Button(action: {
                            (backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_FINDMYPHONE_REQUEST] as! FindMyPhone).sendFindMyPhoneRequest()
                        }, label: {
                            HStack {
                                Text("DevicesDetailView.Actions.RingDevice")
                                Image(systemName: "bell")
                            }
                        })
                    }
                    
                    Button(action: {
                        showingPluginSettingsView = true
                    }, label: {
                        HStack {
                            Image(systemName: "dot.arrowtriangles.up.right.down.left.circle")
                            Text("DevicesDetailView.Actions.PluginSettings")
                        }
                    })
                    
                    Button(action: {
                        showingEncryptionInfo = true
                    }, label: {
                        HStack {
                            Text("DevicesDetailView.Actions.EncryptionInfo")
                            Image(systemName: "lock.doc")
                        }
                    })
                    
                    Button(action: {
                        showingUnpairConfirmationAlert = true
                    }, label: {
                        HStack {
                            Text("DevicesDetailView.Actions.Unpair")
                            Image(systemName: "wifi.slash")
                        }
                    })
                    
                } label: {
                    Image(systemName: "ellipsis.circle")
                }
            }())
            .fileImporter(isPresented: $showingFilePicker, allowedContentTypes: allUTTypes, allowsMultipleSelection: true) { result in
                do {
                    chosenFileURLs = try result.get()
                } catch {
                    print("Document Picker Error")
                }
                if (chosenFileURLs.count > 0) {
                    (backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_SHARE] as! Share).prepAndInitFileSend(fileURLs: chosenFileURLs)
                }
            }
            .onAppear() {
                connectedDevicesViewModel.currDeviceDetailsView = self
//                (backgroundService._devices[detailsDeviceId] as! Device)._backgroundServiceDelegate = connectedDevicesViewModel
                //print((backgroundService._devices[detailsDeviceId] as! Device)._plugins as Any)
                //print((backgroundService._devices[detailsDeviceId] as! Device)._incomingCapabilities as Any)
                // TODO: use if let as
                if ((backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_RUNCOMMAND] != nil) && backgroundService._devices[detailsDeviceId]!._pluginsEnableStatus[PACKAGE_TYPE_RUNCOMMAND] as! Bool) {
                    (backgroundService._devices[detailsDeviceId]!._plugins[PACKAGE_TYPE_RUNCOMMAND] as! RunCommand).requestCommandList()
                }
            }
        } else {
            VStack {
                Spacer()
                Image(systemName: "wifi.slash")
                    .foregroundColor(.red)
                    .font(.system(size: 40))
                Text("DevicesDetailView.DeviceOffline")
                Spacer()
            }
            // Calling this here will refresh after getting to the DeviceView, a bit of delay b4 the
            // list actually refreshes but still works
//            .onDisappear() {
//                connectedDevicesViewModel.devicesView!.refreshDiscoveryAndList()
//            }
        }
    }
}

//struct DevicesDetailView_Previews: PreviewProvider {
//    static var previews: some View {
//        DevicesDetailView(detailsDeviceIndex: 0)
//    }
//}
