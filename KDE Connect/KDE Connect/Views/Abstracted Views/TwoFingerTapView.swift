/*
 * SPDX-FileCopyrightText: 2021 Lucas Wang <lucas.wang@tuta.io>
 *
 * SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
 */

// Original header below:
//
//  TwoFingerTapView.swift
//  KDE Connect Test
//
//  Created by Lucas Wang on 2021-09-06.
//

import SwiftUI

struct TwoFingerTapView: UIViewRepresentable
{
    var tapCallback: (UITapGestureRecognizer) -> Void
    
    typealias UIViewType = UIView
    
    func makeCoordinator() -> TwoFingerTapView.Coordinator
    {
        Coordinator(tapCallback: self.tapCallback)
    }
    
    func makeUIView(context: UIViewRepresentableContext<TwoFingerTapView>) -> UIView
    {
        let view = UIView()
        let TwoFingerTapGestureRecognizer = UITapGestureRecognizer(target: context.coordinator, action: #selector(Coordinator.handleTap(sender:)))
        
        // Set number of touches.
        TwoFingerTapGestureRecognizer.numberOfTouchesRequired = 2
        
        view.addGestureRecognizer(TwoFingerTapGestureRecognizer)
        
        let instructionLabel: UILabel = UILabel()
        instructionLabel.translatesAutoresizingMaskIntoConstraints = false
        instructionLabel.textAlignment = .right
        instructionLabel.text = String(localized: "TwoFingerTapView.InstructionLabel")
        instructionLabel.numberOfLines = 12
        instructionLabel.textAlignment = .center
        view.addSubview(instructionLabel)
        
        NSLayoutConstraint.activate([
            instructionLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            instructionLabel.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.8),
            instructionLabel.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: UIScreen.main.bounds.height / 4),
        ])
        
        return view
    }
    
    func updateUIView(_ uiView: UIView, context: UIViewRepresentableContext<TwoFingerTapView>)
    {
    }
    
    class Coordinator
    {
        var tapCallback: (UITapGestureRecognizer) -> Void
        
        init(tapCallback: @escaping (UITapGestureRecognizer) -> Void)
        {
            self.tapCallback = tapCallback
        }
        
        @objc func handleTap(sender: UITapGestureRecognizer)
        {
            self.tapCallback(sender)
        }
    }
}
