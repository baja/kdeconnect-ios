//
//  SettingsView.swift
//  KDE Connect Test
//
//  Created by Lucas Wang on 2021-06-17.
//

import SwiftUI

struct SettingsView: View {
    @ObservedObject private var selfDeviceDataForSettings: SelfDeviceData = selfDeviceData
    
    var body: some View {
        List {
            // These could go in sections to give them each descriptions and space
            Section(header: Text("SettingsView.HostDeviceSettings.Title")) {
                NavigationLink(
                    destination: SettingsDeviceNameView(deviceName: $selfDeviceDataForSettings.deviceName),
                    label: {
                        HStack {
                            Label("SettingsView.HostDeviceSettings.DeviceName", systemImage: "iphone")
                                .accentColor(.primary)
                            Spacer()
                            Text(selfDeviceData.deviceName)
                                .foregroundColor(.secondary)
                        }
                    })
                
                NavigationLink(
                    destination: SettingsChosenThemeView(chosenTheme: $selfDeviceDataForSettings.chosenTheme),
                    label: {
                        HStack {
                            Label("SettingsView.HostDeviceSettings.AppTheme", systemImage: "lightbulb")
                                .accentColor(.primary)
                            Spacer()
                            Text(selfDeviceData.chosenTheme)
                                .foregroundColor(.secondary)
                        }
                    })
                
                NavigationLink(
                    destination: SettingsAdvancedView(),
                    label: {
                        Label("SettingsView.HostDeviceSettings.AdvancedSettings", systemImage: "wrench.and.screwdriver")
                            .accentColor(.primary)
                    })
            }
            
            Section(header: Text("SettingsView.ExternalLinks.Title")) {
                Label {
                    Link("SettingsView.ExternalLinks.WikiAndManual", destination: URL(string: "https://userbase.kde.org/KDEConnect")!)
                } icon: {
                    Image(systemName: "books.vertical")
                        .accentColor(.primary)
                }
                
                Label {
                    Link("SettingsView.ExternalLinks.ReportBug", destination: URL(string: "https://bugs.kde.org/enter_bug.cgi?product=kdeconnect&component=ios-application")!)
                } icon: {
                    Image(systemName: "ladybug")
                        .accentColor(.primary)
                }
                
                Label {
                    Link("SettingsView.ExternalLinks.Donate", destination: URL(string: "https://kde.org/community/donations/")!)
                } icon: {
                    Image(systemName: "dollarsign.square")
                        .accentColor(.primary)
                }
                
                Label {
                    Link("SettingsView.ExternalLinks.SourceCode", destination: URL(string: "https://invent.kde.org/network/kdeconnect-ios")!)
                } icon: {
                    Image(systemName: "chevron.left.forwardslash.chevron.right")
                        .accentColor(.primary)
                }
                
                Label {
                    Link("SettingsView.ExternalLinks.Licenses", destination: URL(string: "https://invent.kde.org/network/kdeconnect-ios/-/blob/master/License.md")!)
                } icon: {
                    Image(systemName: "magazine")
                        .accentColor(.primary)
                }
            }
        }
        .environment(\.defaultMinListRowHeight, 50) // TODO: make this dynamic with GeometryReader???
        .navigationTitle("SettingsView.NavigationTitle")
    }
}

//struct SettingsView_Previews: PreviewProvider {
//    static var previews: some View {
//        SettingsView()
//    }
//}
