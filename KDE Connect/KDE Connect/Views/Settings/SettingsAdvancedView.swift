//
//  SettingsAdvancedView.swift
//  KDE Connect Test
//
//  Created by Lucas Wang on 2021-09-12.
//

import SwiftUI

struct SettingsAdvancedView: View {
    var body: some View {
        List {
            Section(header: Text("SettingsAdvancedView.DangerousOptions.Header"), footer: Text("SettingsAdvancedView.DangerousOptions.Footer")) {
                Button(action: {
                    notificationHapticsGenerator.notificationOccurred(.warning)
                    certificateService.deleteAllItemsFromKeychain()
                    UserDefaults.standard.removeObject(forKey: "savedDevices")
                }, label: {
                    HStack {
                        Image(systemName: "delete.right")
                        VStack(alignment: .leading) {
                            Text("SettingsAdvancedView.DangerousOptions.EraseCache.Title")
                                .font(.headline)
                            Text("SettingsAdvancedView.DangerousOptions.EraseCache.Message")
                                .font(.caption)
                        }
                    }
                    .accentColor(.red)
                })
                
                Button(action: {
                    notificationHapticsGenerator.notificationOccurred(.warning)
                    certificateService.deleteHostCertificateFromKeychain()
                }, label: {
                    HStack {
                        Image(systemName: "delete.right")
                        VStack(alignment: .leading) {
                            Text("SettingsAdvancedView.DangerousOptions.DeleteCertificate.Title")
                                .font(.headline)
                            Text("SettingsAdvancedView.DangerousOptions.DeleteCertificate.Message")
                                .font(.caption)
                        }
                    }
                    .accentColor(.red)
                })
            }
        }
        .navigationBarTitle("SettingsAdvancedView.NavigationTitle", displayMode: .inline)
    }
}

//struct SettingsAdvancedView_Previews: PreviewProvider {
//    static var previews: some View {
//        SettingsAdvancedView()
//    }
//}
